﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace reloj_checador
{
    public partial class Reloj : Form
    {
        DateTime infoEntrada = new DateTime();
        DateTime datoSalida = new DateTime();
        DateTime borrart = new DateTime();
        string sgp;

        //banderas de prueba
        bool b1 = false;
        bool b2 = false;
        int flag = 0;

        public string Path = @"C:\Users\SEBASTIAN\desktop\ExamenRegistros.txt";

        public Reloj()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void btnIngresar_Click(object sender, EventArgs e)
        {
            sgp = comboBox1.Text;
            if (!File.Exists(Path))
            {
                using (StreamWriter sw = File.CreateText(Path))
                {
                    sw.WriteLine(sgp);
                    sw.WriteLine(infoEntrada);

                }
            }
            else
            {
                string append = "";
                if (flag == 1)
                {
                    append = "Entrada\n" + sgp + "\n" + infoEntrada + Environment.NewLine;
                }
                else if (flag == 2)
                {
                    append = "Salida\n" + sgp + "\n" + datoSalida + Environment.NewLine;
                }
                File.AppendAllText(Path, append);
                infoEntrada = borrart;
            }
        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            string readText = File.ReadAllText(Path);
            MessageBox.Show(readText);
            //MessageBox.Show("El empleado es : " + g + "\nLa fecha de entrada es : " + datoEntrada+"\n");
        }

        private void rtbnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            infoEntrada = dateTimePicker1.Value;
            btnRegistro.Enabled = true;
            btnRevision.Enabled = true;
            b1 = true;
            flag = 1;
        }

        private void rbtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            datoSalida = dateTimePicker1.Value;
            btnRegistro.Enabled = true;
            btnRevision.Enabled = true;
            b2 = true;
            flag = 2;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            infoEntrada = dateTimePicker1.Value;
        }
    }
}
