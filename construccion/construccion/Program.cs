﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chalanes
{
    class Program
    {
        private static Semaphore deepAlbañil;
        private static int second;
        private static int to = 0;
        private static int pud = 0;
        private static int time = 0;
        private static int home = 0;
        private static int habi = 0;
        private static int baños = 0;
        private static void ingCivil(object albañil)
        {
            Console.WriteLine("Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("albañil {0} esperando turno...", albañil);
            deepAlbañil.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref second, 100);
            Console.WriteLine("albañil {0} obtiene turno...", albañil);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("albañil {0} termina turno...", albañil);
            Console.WriteLine("albañil {0} libera su lugar {1}",
                albañil, deepAlbañil.Release());
        }

        public static async void ConstruirCasas(int cantidad, int Habitaciones, int baños)
        {
            Random rand = new Random();
            Task[] Casas = new Task[cantidad];
            int cont = 0;
            Action<object> action = (object obj) =>
            {
                Console.WriteLine(" La casa {0} ya esta lista para construir ", Task.CurrentId);
            };

            for (int i = 0; i < Casas.Length; i++)
            {

                Casas[i] = new Task(action, new string('1', i + 1));
                Console.WriteLine("fincando casa {0} ", Task.CurrentId);
                if (cont != 0)
                {
                    Habitaciones = rand.Next(1, 10);
                    baños = rand.Next(1, 5);

                }
                for (int j = 0; j < Habitaciones; j++)
                {
                    Console.WriteLine(" contruyendo habitaciones ");
                }
                for (int h = 0; h < baños; h++)
                {
                    Console.WriteLine(" sentando baños");
                }
                Casas[i].Start();
                Thread.Sleep(500);
                cont++;


            }

            Console.WriteLine("Casas terminadas");

            await Task.WhenAll(Casas);

        }


        public static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos albañiles llegaron a chambear?");
            to = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos pueden chambear?");
            pud = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos se van contruir?");
            time = Convert.ToInt32(Console.ReadLine()) * 1000;

            Console.WriteLine("¿cuantas casas se van a construir?");
            home = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿cuantas habitaciones se van a construir?");
            habi = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿cuantas baños se van a construir?");
            baños = Convert.ToInt32(Console.ReadLine());

            deepAlbañil = new Semaphore(0, pud);


            Console.WriteLine("Inicia la construcción");

            for (int i = 1; i <= pud; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ingCivil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }
            ConstruirCasas(home, habi, baños);

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            deepAlbañil.Release(pud);


            Thread.Sleep(time);
            Console.WriteLine("Se terminó la construcción");
            Console.ReadLine(); 
        }
    }
}